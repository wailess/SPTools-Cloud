package com.tools.meizi.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONObject;
import com.tools.common.core.constant.SystemConstant;
import com.tools.common.core.model.Result;
import com.tools.common.core.util.DateUtils;
import com.tools.meizi.entity.Girl;
import com.tools.meizi.service.GirlService;
import io.swagger.annotations.Api;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * 图片后台管理
 */
@Api(tags ="图片后台管理")
@RestController
@RequestMapping("girl")
public class GirlController {

    @Autowired
    private GirlService meiZiService;
    @Value("${file.path}")
    private String filePath;
    @Value("${aliyun.oss.url}")
    private String ossUrl;

    /**
     * 文件上传
     */
    @PostMapping("/upload")
    public JSONObject upload(@RequestParam(value = "editormd-image-file")MultipartFile file,
                             HttpServletRequest request) {
        JSONObject res = new JSONObject();
        try {
            if(file!=null){
                String status = request.getParameter("status");
                File parentFile = createParentFile();
                String fileName = file.getOriginalFilename();
                String suffix = fileName.substring(fileName.lastIndexOf("."));
                String uuid = IdUtil.simpleUUID();
                fileName = uuid + suffix;
                File fromPic = new File(parentFile,fileName);
                FileUtil.writeFromStream(file.getInputStream(), fromPic);
                /**
                 * 缩放
                 */
                fileName = "zoom_"+uuid + suffix;
                File toPic = new File(parentFile,fileName);
                /**
                 * scale 比例
                 * outputQuality 质量
                 */
                Thumbnails.of(fromPic).scale(0.5f).outputQuality(1f).toFile(toPic);
                String fileDay = DateUtil.thisYear()+"/"+(DateUtil.thisMonth()+1)+"/"+DateUtil.thisDayOfMonth();
                String imagePath = SystemConstant.FILE + "/" + fileDay+"/"+fileName;
                /**
                 * 入库
                 */
                Girl image = new Girl();
                image.setUuid(uuid);
                image.setUrl(imagePath);
                image.setOssUrl(fileDay+"/"+fileName);
                image.setGmtCreate(DateUtils.getTimestamp());
                image.setStatus(Short.parseShort(status));
                meiZiService.upload(image,toPic,fromPic);
                res.put("url",ossUrl+"/"+fileDay+"/"+fileName);
                res.put("success", 1);
                res.put("message", "upload success!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 创建多级文件夹
     * @return
     */
    public File createParentFile(){
        File parentFile = new File(filePath+ SystemConstant.SF_FILE_SEPARATOR+ DateUtil.thisYear());
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        parentFile = new File(parentFile,(DateUtil.thisMonth()+1)+"");
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        parentFile = new File(parentFile,DateUtil.thisDayOfMonth()+"");
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        return parentFile;
    }

    /**
     * 用户列表
     */
    @PostMapping("/list")
    public Result list(Girl girl){
        return meiZiService.list(girl);
    }

    /**
     * 消息
     * @return
     */
    @PostMapping("delete")
    public Result delete(Long id) {
        return meiZiService.delete(id);
    }

    /**
     * 删除全部
     */
    @PostMapping("/removeAll")
    public Result removeAll(Short status){
        return meiZiService.removeAll(status);
    }

    /**
     * 启用恢复
     */
    @PostMapping("/resume")
    public Result resume(Short status,Long id){
        return meiZiService.resume(status,id);
    }
}
