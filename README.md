## 介绍

这就是个妹子图微服务学习案例，仅供参考，持续更新。

- 前端水平太菜，伪前后端分离，只是把前端页面抽离出来单独部署。
- 后端采用 `SpringCloud`全家桶，部分组件采用 `Alibaba`。
- 权限认证采用`JWT`+自定义注解(`RBAC`)实现，抛弃相对比较重的 `SpringSecurity`。
- 单机版本请移步[炒鸡工具箱](https://gitee.com/52itstyle/SPTools)
- 妹子图小程序请移步[爪哇妹](https://gitee.com/52itstyle/mzitu)


## 软件架构


![输入图片说明](https://images.gitee.com/uploads/images/2020/0701/204943_c15f8b2c_87650.png "屏幕截图.png")


## 内置功能

- 组织机构：机构管理、用户管理、角色管理、行政区域。

- 系统监控：系统日志、在线用户，后期会慢慢追加完善。

- 应用管理：任务调度、邮件管理、图片管理、文章管理、人工智能，每个模块只需要你稍作修改就可以打造成一个项目了。

- 系统管理：敏捷开发、系统菜单、全局配置、在线代码编辑器，小伙伴们只需要设计好表结构，三秒中就能撸出一个增删查改的模块。


## 核心依赖

| 依赖             | 版本         |
|----------------|---------------|
| Spring Boot    | 2.2.5.RELEASE |
| Spring Cloud   | Hoxton.SR4    |
| Nacos          | 1.3.0         |
| OpenResty      | 1.17.8.2      |
| Redis          | 6.0           |
| MySql          | 5.7.17        |
| Ant Design Vue | 探索领域      |

## 模块

| 模块| 功能描述|端口|
|----------------|---------------|---------------|
| tools-gateway    | 网关 | 8080 |
| tools-monitor   | 监控    | 8081|
| tools-sys| 系统管理        | 8082 |
| tools-meizi| 妹子图           | 8083|
| tools-ui| 前端           | 8084|
| tools-common| 通用模块(鉴权、缓存、数据库、文件等等)| ****|

## 地址

| 模块| 地址|功能描述|
|----------------|---------------|---------------|
| 后台管理    | https://tools.cloudbed.vip/login.html | 后台管理 |
| 接口管理   | http://localhost:8080/doc.html    | 接口地址，生产不对外开放|
| 妹子图接口| https://meizi.cloudbed.vip        | 单独配置服务，不走网关 |

## 安装教程


- 安装 `nacos`，相关配置见部署->配置文件夹，请自行配置。

- 创建数据库 `tools_cloud`，并导入部署->数据库文件数据。

- 如果只是想测试不想安装`Nacos`，撸主也提供了免费的注册中心，请加企鹅群 `933593697 `获取。


## 演示图

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/191648_a6db8c4c_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/191848_edcafb7e_87650.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192547_af73469b_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192604_82b4bed9_87650.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192620_83bf77d3_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192631_b52f7018_87650.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192646_28fc8ad8_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192659_f344d433_87650.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192712_f1276903_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192732_ae0d76b2_87650.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192745_4e6354f7_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192800_eefa1344_87650.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192813_f7600d93_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192824_75b4bb38_87650.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192839_205e772d_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0512/192851_30924c45_87650.png"/></td>
    </tr>
 </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0718/143956_fc052030_87650.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0718/144025_f67920b5_87650.png"/></td>
    </tr>
</table>


## 部署参考

[Nacos安装配置说明](https://gitee.com/52itstyle/SPTools-Cloud/blob/master/%E9%83%A8%E7%BD%B2/Nacos%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E.md)

[OpenResty安装配置说明](https://gitee.com/52itstyle/SPTools-Cloud/blob/master/%E9%83%A8%E7%BD%B2/OpenResty%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E.md)

[Redis单机安装配置说明](https://gitee.com/52itstyle/SPTools-Cloud/blob/master/%E9%83%A8%E7%BD%B2/Redis%E5%AE%89%E8%A3%85%E8%AF%B4%E6%98%8E.md)

[Redis集群安装配置说明](https://gitee.com/52itstyle/SPTools-Cloud/blob/master/%E9%83%A8%E7%BD%B2/Redis%E9%9B%86%E7%BE%A4%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E.md)

[Docker容器化部署](https://gitee.com/52itstyle/SPTools-Cloud/blob/master/%E9%83%A8%E7%BD%B2/Docker%E5%AE%B9%E5%99%A8%E5%8C%96%E9%83%A8%E7%BD%B2%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E.md)

## 炒鸡工具箱交流群

企鹅群：933593697

![输入图片说明](https://images.gitee.com/uploads/images/2020/0521/174508_3fc74b80_87650.png "超级工具箱群二维码.png")

## 推荐


- 秒杀案例：https://gitee.com/52itstyle/spring-boot-seckill

- 任务调度：https://gitee.com/52itstyle/spring-boot-quartz

- 邮件服务：https://gitee.com/52itstyle/spring-boot-mail

- 搜索服务：https://gitee.com/52itstyle/spring-boot-elasticsearch

- 三大支付：https://gitee.com/52itstyle/spring-boot-pay


作者： 小柒2012

欢迎关注： https://blog.52itstyle.vip
