package com.tools.sys.repository;

import com.tools.sys.entity.SysRoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * sys_role_menu Repository
 * Created by 小柒2012
 * Sun Oct 27 13:02:47 CST 2019
*/ 
@Repository 
public interface SysRoleMenuRepository extends JpaRepository<SysRoleMenu, Long> {
}

