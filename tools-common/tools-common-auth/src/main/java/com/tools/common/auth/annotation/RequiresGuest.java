package com.tools.common.auth.annotation;

import java.lang.annotation.*;

/**
 * 不需要认证注解
 * @author 小柒2012
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequiresGuest {

}
