package com.tools.sys.service;

import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysOrg;

public interface SysOrgService {

    Result list(SysOrg org);

    Result select(Long parentId);

    Result save(SysOrg org);

    Result delete(Long orgId);

}
