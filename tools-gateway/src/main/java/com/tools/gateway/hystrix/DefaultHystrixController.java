package com.tools.gateway.hystrix;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 降级处理
 */
@RestController
public class DefaultHystrixController {

    @RequestMapping("/fallback")
    public Map<String,String> fallback(){
        Map<String,String> map = new HashMap<>(8);
        map.put("code","fail");
        map.put("msg","服务异常");
        return map;
    }
}
