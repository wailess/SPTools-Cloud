package com.tools.meizi.service.impl;

import com.tools.common.core.constant.SystemConstant;
import com.tools.common.core.model.Result;
import com.tools.common.dynamicquery.DynamicQuery;
import com.tools.meizi.entity.Recommend;
import com.tools.meizi.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecommendServiceImpl implements RecommendService {

    @Autowired
    private DynamicQuery dynamicQuery;

    @Override
    public Result list(Integer pageSize, Integer pageNo) {
        pageNo = pageNo<=0?0:pageNo-1;
        String nativeSql = "SELECT * FROM app_recommend WHERE status=? ORDER BY gmt_create desc";
        Pageable pageable = PageRequest.of(pageNo,pageSize);
        List<Recommend> list =
                dynamicQuery.nativeQueryPagingList(Recommend.class,pageable,nativeSql,new Object[]{SystemConstant.DELETE_STATUS_NO});
        return Result.ok(list);
    }

    @Override
    @Cacheable(cacheNames = {"recommend"})
    public Result get(String uuid) {
        String nativeSql = "SELECT * FROM app_recommend WHERE uuid=?";
        Recommend recommend =
                dynamicQuery.nativeQuerySingleResult(Recommend.class,nativeSql,new Object[]{uuid});
        return Result.ok(recommend);
    }
    @Override
    public Result today() {
        String nativeSql = "SELECT * FROM app_recommend WHERE hot=?";
        Recommend recommend =
                dynamicQuery.nativeQuerySingleResult(Recommend.class,nativeSql,new Object[]{SystemConstant.HOT_STATUS_YES});
        return Result.ok(recommend);
    }
}
