## 简介

设计目标，是期望nacos存在两种数据存储模式，一种是现在的方式，数据存储在外置数据源（关系型数据库）；第二种方式是内嵌存储数据源（Apache Derby）。用户能够使用命令行参数配置的方式，随意使用这两种数据存储模式。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0624/220944_b75dc332_87650.png "屏幕截图.png")

将一次请求操作涉及的SQL上下文按顺序保存起来。然后通过一致协议层将本次请求涉及的SQL上下文进行同步，然后每个节点将其解析并重新按顺序在一次数据库会话中执行。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0624/221304_8c28f91e_87650.png "屏幕截图.png")

## 下载

实在是太难了，龟速下载，这里撸主提供了下载地址，见软件。

```
https://github.com/alibaba/nacos/releases/tag/1.3.0
```

## 安装

解压：

```
tar -xvf nacos-server-1.3.0.tar.gz
```
单机内嵌数据库启动：

```
./startup.sh -m standalone -p embedded
```

## 集群模式


在nacos的conf目录下，创建cluster.conf配置文件，每行为ip:port。注意，ip为内网地址。


```
#it is ip
#example
192.168.16.101:8847
192.168.16.102:8847
192.168.16.103:8847
```

集群内嵌数据库依次启动：

```
./startup.sh -p embedded
```

## 开启验证

配置文件：`/nacos/conf/application.properties` 修改：

```
### If turn on auth system:
nacos.core.auth.enabled=true
```

项目配置：

```
 spring:
  cloud:
    nacos:
      config:
        server-addr: 192.168.1.24:8848
        username: nacos
        password: 123456
      discovery:
        server-addr: 192.168.1.24:8848
        username: nacos
        password: 123456

```


## 参考

https://nacos.io/zh-cn/blog/nacos-1.3.0-design.html

https://www.cnblogs.com/ylcc-zyq/p/13140561.html
