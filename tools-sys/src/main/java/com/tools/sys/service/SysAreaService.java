package com.tools.sys.service;

import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysArea;

import java.util.List;

public interface SysAreaService {

    Result list(SysArea area);

    Result save(SysArea area);

    List<SysArea> select(String parentCode);

    Result delete(String areaCode);

}
