package com.tools.sys.service;

import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysUser;

import java.util.List;

public interface SysUserService {

    Result save(SysUser user);

    Result get(Long userId);

    Result delete(Long userId);

    SysUser getUser(String username);

    Result list(SysUser user);

    List<Object> listUserRoles(Long userId);

    List<Object> listUserPerms(Long userId);

    Result updatePwd(SysUser user);
}
