package com.tools.sys.controller;

import com.tools.common.core.config.AbstractController;
import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysRole;
import com.tools.sys.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 角色管理
 * 爪哇笔记：https://blog.52itstyle.vip
 */
@Api(tags ="角色管理")
@RestController
@RequestMapping("/role")
public class RoleController extends AbstractController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 角色列表
     */
    @ApiOperation(value="角色列表")
    @PostMapping("/list")
    public Result list(SysRole role){
        return sysRoleService.list(role);
    }

    /**
     * 角色选择
     */
    @ApiOperation(value="列出所有角色")
    @PostMapping("/select")
    public Result select(){
        return sysRoleService.select();
    }

    /**
     * 角色选择
     */
    @PostMapping("/selectByUser")
    public Result selectByUser(){
        return sysRoleService.select();
    }

    /**
     * 保存
     */
    @ApiOperation(value="保存")
    @PostMapping("/save")
    public Result save(@RequestBody SysRole role){

        return sysRoleService.save(role);
    }

    /**
     * 删除
     */
    @ApiOperation(value="删除")
    @PostMapping("/delete")
    public Result delete(Long roleId){
        return sysRoleService.delete(roleId);
    }

    /**
     * 根据角色ID获取菜单
     */
    @ApiOperation(value="根据角色ID获取菜单")
    @PostMapping("/getMenu")
    public Result getMenu(Long roleId){
        return sysRoleService.getMenu(roleId);
    }

    /**
     * 根据角色保存菜单
     */
    @ApiOperation(value="根据角色保存菜单")
    @PostMapping("/saveMenu")
    public Result saveMenu(@RequestBody SysRole role){
        return sysRoleService.saveMenu(role);
    }

    /**
     * 根据角色ID获取机构
     */
    @PostMapping("/getOrg")
    @ApiOperation(value="根据角色ID获取机构")
    public Result getOrg(Long roleId){
        return sysRoleService.getOrg(roleId);
    }

    /**
     * 根据角色保存机构
     */
    @PostMapping("/saveOrg")
    @ApiOperation(value="根据角色保存机构")
    public Result saveOrg(@RequestBody SysRole role){
        return sysRoleService.saveOrg(role);
    }
}
