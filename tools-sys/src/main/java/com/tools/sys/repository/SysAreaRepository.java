package com.tools.sys.repository;

import com.tools.sys.entity.SysArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * sys_area Repository
 * Created by 小柒2012
 * Sun Oct 27 12:56:55 CST 2019
*/ 
@Repository 
public interface SysAreaRepository extends JpaRepository<SysArea, Long> {
}

