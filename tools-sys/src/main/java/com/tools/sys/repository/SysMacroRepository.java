package com.tools.sys.repository;

import com.tools.sys.entity.SysMacro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * sys_macro Repository
 * Created by 小柒2012
 * Sun Oct 27 13:01:25 CST 2019
*/ 
@Repository 
public interface SysMacroRepository extends JpaRepository<SysMacro, Long> {
}

