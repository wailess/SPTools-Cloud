package com.tools.meizi.service.impl;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaSubscribeData;
import cn.binarywang.wx.miniapp.bean.WxMaSubscribeMessage;
import com.tools.common.core.model.Result;
import com.tools.common.core.util.DateUtils;
import com.tools.common.dynamicquery.DynamicQuery;
import com.tools.meizi.model.PushModel;
import com.tools.meizi.service.PushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 推送消息
 * 参数说明：https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
 */
@Service
public class PushServiceImpl implements PushService {

    @Autowired
    private WxMaService wxService;

    @Autowired
    private DynamicQuery dynamicQuery;

    private static int corePoolSize = Runtime.getRuntime().availableProcessors();
    private static ThreadPoolExecutor executor  =
            new ThreadPoolExecutor(corePoolSize, corePoolSize+1, 10l, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<>(100));


    @Override
    public Result push() {
        PushModel model = new PushModel();
        model.setPage("pages/history/history");
        model.setOpenid("oCay55YrmfTa5E-NI9pmE3ZiXeyc");
        model.setTemplateId("Qwe3JQrNVwVqBpfeqSxOtp-PdpbCHqYgzq1OJg7HaEo");
        model.setTitle("妹纸在吃的道路上");
        model.setTime(DateUtils.getTime());
        model.setAuthor("小柒");
        push(model);
        return Result.ok();
    }

    public void push(PushModel model) {
        Runnable task = () -> {
            WxMaSubscribeMessage subscribeMessage = new WxMaSubscribeMessage();
            //跳转小程序页面路径
            subscribeMessage.setPage(model.getPage());
            //模板消息id
            subscribeMessage.setTemplateId(model.getTemplateId());
            //给谁推送 用户的openid
            subscribeMessage.setToUser(model.getOpenid());
            ArrayList<WxMaSubscribeData> wxMaSubscribeData = new ArrayList<>();

            //第一个内容：更新内容
            WxMaSubscribeData wxMaSubscribeData1 = new WxMaSubscribeData();
            wxMaSubscribeData1.setName("thing1");
            wxMaSubscribeData1.setValue(model.getTitle());
            wxMaSubscribeData.add(wxMaSubscribeData1);

            // 第二个内容：时间
            WxMaSubscribeData wxMaSubscribeData2 = new WxMaSubscribeData();
            wxMaSubscribeData2.setName("date2");
            wxMaSubscribeData2.setValue(model.getTime());
            wxMaSubscribeData.add(wxMaSubscribeData2);
            // 第三个内容：作者
            WxMaSubscribeData wxMaSubscribeData3 = new WxMaSubscribeData();
            wxMaSubscribeData3.setName("name3");
            wxMaSubscribeData3.setValue(model.getAuthor());
            wxMaSubscribeData.add(wxMaSubscribeData3);
            //把集合给大的data
            subscribeMessage.setData(wxMaSubscribeData);
            try {
                //进行推送
                wxService.getMsgService().sendSubscribeMsg(subscribeMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        executor.execute(task);
    }
}
